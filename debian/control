Source: node-stream-to-observable
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>
Section: javascript
Testsuite: autopkgtest-pkg-nodejs
Priority: optional
Build-Depends:
 debhelper-compat (= 13)
 , dh-sequence-nodejs
 , node-tape <!nocheck>
 , node-zen-observable <!nocheck>
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/js-team/node-stream-to-observable
Vcs-Git: https://salsa.debian.org/js-team/node-stream-to-observable.git
Homepage: https://github.com/jamestalmage/stream-to-observable#readme
Rules-Requires-Root: no

Package: node-stream-to-observable
Architecture: all
Depends:
 ${misc:Depends}
 , node-zen-observable
Multi-Arch: foreign
Description: Convert Node Streams into ECMAScript-Observables
 Observables have much in common with Streams, in that they both represent data
 that arrives over time. Most Observable implementations provide expressive
 methods for filtering and mutating incoming data. Methods like `.map()`,
 `.filter()`, and `.forEach` behave very similarly to their Array counterparts,
 so using Observables can be very intuitive.
 .
 Node.js is an event-based server-side JavaScript engine.
